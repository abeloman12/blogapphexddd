package test

import (
	"blogapp/internal/constant"
	"blogapp/internal/constant/model"
	"blogapp/internal/http/rest/server/user"
	userModule "blogapp/internal/module/user"
	"blogapp/internal/storage/persistence"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
)

func Test_GetUsers_EmptyResult(t *testing.T) {
	err := godotenv.Load("../.env")
	if err != nil {
		log.Fatal("Error loading .env file " + err.Error())
	}

	db, err := constant.DBConnection()
	if err != nil {
		panic(err)
	}

	req, w := setGetUsersRouter(db)
	dbCon, _ := db.DB()
	defer dbCon.Close()

	a := assert.New(t)
	a.Equal(http.MethodGet, req.Method, "HTTP request method error")
	a.Equal(http.StatusOK, w.Code, "HTTP request status code error")

	body, err := ioutil.ReadAll(w.Body)
	if err != nil {
		a.Error(err)
	}

	actual := model.User{}
	if err := json.Unmarshal(body, &actual); err != nil {
		a.Error(err)
	}

	expected := model.User{}
	a.Equal(expected, actual)
}

func setGetUsersRouter(db *gorm.DB) (*http.Request, *httptest.ResponseRecorder) {
	r := gin.New()
	userRepository := persistence.UserInit(db)
	userUseCase := userModule.Initialize(userRepository)
	userHandler := user.NewUserHandler(userUseCase)

	r.GET("/api/users/", userHandler.GetUsers)
	req, err := http.NewRequest(http.MethodGet, "/api/users/", nil)
	if err != nil {
		panic(err)
	}

	req.Header.Set("Content-Type", "application/json")

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return req, w
}
