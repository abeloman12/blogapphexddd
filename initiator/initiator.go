package initiator

import (
	"blogapp/internal/constant"
	"blogapp/internal/glue/routing"
	blogHandler "blogapp/internal/http/rest/server/blog"
	subHandler "blogapp/internal/http/rest/server/sub"
	userHandler "blogapp/internal/http/rest/server/user"
	billModule "blogapp/internal/module/billing"
	blogModule "blogapp/internal/module/blog"
	subModule "blogapp/internal/module/sub"
	userModule "blogapp/internal/module/user"
	"blogapp/internal/storage/persistence"
	"log"

	"github.com/joho/godotenv"
	"github.com/robfig/cron/v3"
)

func Initiate() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file " + err.Error())
	}

	db, err := constant.DBConnection()
	if err != nil {
		panic(err)
	}

	db = db.Debug()

	userPersistence := persistence.UserInit(db)

	if err := userPersistence.MigrateUser(); err != nil {
		log.Fatal("User Migrate Error", err)
	}

	userUseCase := userModule.Initialize(userPersistence)
	userHandler := userHandler.NewUserHandler(userUseCase)

	blogPersistence := persistence.BlogInit(db)

	if err := blogPersistence.MigrateBlog(); err != nil {
		log.Fatal("Blog Migrate Error", err)
	}

	blogUseCase := blogModule.Initialize(blogPersistence)
	blogHandler := blogHandler.NewBlogHandler(blogUseCase)

	subPersistence := persistence.SubscriptionInit(db)

	if err := subPersistence.MigrateSubscription(); err != nil {
		log.Fatal("Subscription Migrate Error", err)
	}

	subUseCase := subModule.Initialize(subPersistence)
	subHandler := subHandler.NewSubscriptionHandler(subUseCase)

	appHandler := routing.AppHandler{
		UserHandler: userHandler,
		BlogHandler: blogHandler,
		SubHandler:  subHandler,
	}

	billingPersistence := persistence.BillingInit(db)

	if err := billingPersistence.MigrateBilling(); err != nil {
		log.Fatal("Billing Migrate Error", err)
	}

	cron := cron.New()
	billModule.InitCalculateBill(billingPersistence, userPersistence, subPersistence, cron)
	cron.Start()

	routing.SetupRoutes(db, appHandler)

}
