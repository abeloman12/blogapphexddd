package billing

import (
	"time"

	"blogapp/internal/constant/model"
)

func (s service) CalculateBilling() {
	s.cron.AddFunc("0 30 * * * *", func() {
		users, err := s.userPersistence.Users()

		if err == nil {
			for _, user := range users {
				subscription := model.Subscription{SubscriberID: int(user.ID)}
				subCount, _ := s.subPersistence.SubscriptionCount(subscription)

				billing := model.Billing{}

				billing.UserID = int(user.ID)
				billing.Month = time.Now().Month().String()
				billing.Year = time.Now().Year()
				billing.Amount = float32(model.BASEPRICE + (subCount * model.SUBPRICE))

				s.billingPersistence.StoreBilling(billing)
			}
		}
	})
}
