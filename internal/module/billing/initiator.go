package billing

import (
	"blogapp/internal/storage/persistence"

	"github.com/robfig/cron/v3"

	"time"

	"blogapp/internal/constant/model"
)

type UseCase interface {
	CalculateBilling()
}

type service struct {
	billingPersistence persistence.BillingPersistence
	userPersistence    persistence.UserPersistence
	subPersistence     persistence.SubscriptionPersistence
	cron               *cron.Cron
}

func Initialize(
	billingPersistence persistence.BillingPersistence,
	userPersistence persistence.UserPersistence,
	subPersistence persistence.SubscriptionPersistence,
	cron *cron.Cron,
) UseCase {
	return &service{
		billingPersistence: billingPersistence,
		userPersistence:    userPersistence,
		subPersistence:     subPersistence,
		cron:               cron,
	}
}

func InitCalculateBill(billingPersistence persistence.BillingPersistence,
	userPersistence persistence.UserPersistence,
	subPersistence persistence.SubscriptionPersistence,
	cron *cron.Cron) {

	cron.AddFunc("@every 0h30m", func() {
		users, err := userPersistence.Users()
		if err == nil {
			for _, user := range users {
				subscription := model.Subscription{SubscriberID: int(user.ID)}
				subCount, _ := subPersistence.SubscriptionCount(subscription)

				billing := model.Billing{}

				billing.UserID = int(user.ID)
				billing.Month = time.Now().Month().String()
				billing.Year = time.Now().Year()

				billing.Amount = float32(model.BASEPRICE + (subCount * model.SUBPRICE))

				billingPersistence.StoreBilling(billing)
			}
		}
	})

}
