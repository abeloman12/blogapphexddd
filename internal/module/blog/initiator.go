package blog

import (
	"blogapp/internal/constant"
	model "blogapp/internal/constant/model"
	"blogapp/internal/storage/persistence"
)

type UseCase interface {
	GetBlog(blogID string) (*model.Blog, error)
	UpdateBlog(blog model.Blog) (*constant.SuccessData, *constant.ErrorData)
	StoreBlog(blog model.Blog) (*constant.SuccessData, *constant.ErrorData)
	AddBlogImages(blog model.Blog) (*constant.SuccessData, *constant.ErrorData)
	RemoveBlogImages(blog model.Blog) (*constant.SuccessData, *constant.ErrorData)
	GetBlogs() (*constant.SuccessData, *constant.ErrorData)
	DeleteBlog(blogID string) (*constant.SuccessData, *constant.ErrorData)
}

type service struct {
	blogPersistence persistence.BlogPersistence
}

func Initialize(
	blogPersistence persistence.BlogPersistence,
) UseCase {
	return &service{
		blogPersistence: blogPersistence,
	}
}
