package blog

import (
	"blogapp/internal/constant"
	"blogapp/internal/constant/model"
	"errors"

	"net/http"

	"strconv"

	"gorm.io/gorm"
)

func (s service) GetBlog(blogID string) (*model.Blog, error) {
	intBlogID, converstionError := strconv.Atoi(blogID)

	if converstionError != nil || intBlogID == 0 {
		return nil, errors.New("Error converting ID parameter")
	}

	blog, err := s.blogPersistence.Blog(model.Blog{Model: gorm.Model{ID: uint(intBlogID)}})
	if err != nil {
		return nil, errors.New("Error Occurred While Reading Blog")
	}

	return blog, nil
}

func (s service) GetBlogs() (*constant.SuccessData, *constant.ErrorData) {
	blogs, err := s.blogPersistence.Blogs()

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Getting Blogs",
		}
	}

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Getting Blogs",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: blogs,
	}, nil
}

func (s service) UpdateBlog(blog model.Blog) (*constant.SuccessData, *constant.ErrorData) {
	updatedBlog, err := s.blogPersistence.UpdateBlog(blog)

	if err != nil {

		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Updating Blog",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: updatedBlog,
	}, nil
}

func (s service) StoreBlog(blog model.Blog) (*constant.SuccessData, *constant.ErrorData) {
	detail, err := s.blogPersistence.StoreBlog(blog)

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error occurred while adding detail",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: *detail,
	}, nil
}

func (s service) AddBlogImages(blog model.Blog) (*constant.SuccessData, *constant.ErrorData) {
	updatedBlog, err := s.blogPersistence.AddBlogImages(blog)

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Updating Blog",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: updatedBlog,
	}, nil
}

func (s service) RemoveBlogImages(blog model.Blog) (*constant.SuccessData, *constant.ErrorData) {
	updatedBlog, err := s.blogPersistence.RemoveBlogImages(blog)

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Updating Blog",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: updatedBlog,
	}, nil
}

func (s service) DeleteBlog(id string) (*constant.SuccessData, *constant.ErrorData) {
	intID, _ := strconv.Atoi(id)

	err := s.blogPersistence.DeleteBlog(model.Blog{Model: gorm.Model{ID: uint(intID)}})

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error occured while removing blog",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: "Blog Deleted",
	}, nil
}
