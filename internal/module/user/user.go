package user

import (
	"blogapp/internal/constant"
	"blogapp/internal/constant/model"

	"blogapp/repository"
	"net/http"

	"errors"
	"strconv"
	"strings"

	"gorm.io/gorm"
)

func (s service) GetUser(userID string) (*constant.SuccessData, *constant.ErrorData) {
	intUserID, converstionError := strconv.Atoi(userID)
	if converstionError != nil || intUserID == 0 {
		return nil, &constant.ErrorData{
			Code:  http.StatusBadRequest,
			Title: "Error converting ID parameter",
		}
	}
	user, err := s.userPersistence.User(model.User{Model: gorm.Model{ID: uint(intUserID)}})
	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Reading User",
		}
	}
	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: user,
	}, nil
}

func (s service) GetUsers() (*constant.SuccessData, *constant.ErrorData) {

	users, err := s.userPersistence.Users()

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Getting Users",
		}
	}

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Getting Users",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: users,
	}, nil
}

func (s service) GetByEmail(email string) (user *model.User, err error) {
	return s.userPersistence.User(model.User{Email: email})
}

func (s service) UpdateUser(user model.User) (*constant.SuccessData, *constant.ErrorData) {
	_, err := s.userPersistence.UpdateUser(user)
	if err != nil {

		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Updating User",
		}
	}
	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: "User Updated",
	}, nil
}

func (s service) StoreUser(user model.User) (*model.User, error) {
	if !strings.EqualFold(model.SUPERADMIN, user.Role) &&
		!strings.EqualFold(model.AUTHOR, user.Role) &&
		!strings.EqualFold(model.READER, user.Role) {
		return nil, errors.New("Invalid Role")
	}

	user.Role = strings.ToUpper(user.Role)

	if user.Email == "" || !repository.MatchesPattern(user.Email, repository.EmailRX) {
		return nil, errors.New("Invalid Email")
	}

	IsEmailExist, err := s.userPersistence.EmailExists(user.Email)

	if err != nil {
		return nil, errors.New("Error Occurred While Creating User")
	}
	if IsEmailExist {
		return nil, errors.New("Email already taken")
	}

	return s.userPersistence.StoreUser(user)
}

func (s service) DeleteUser(id string) (*constant.SuccessData, *constant.ErrorData) {
	intID, _ := strconv.Atoi(id)
	err := s.userPersistence.DeleteUser(model.User{Model: gorm.Model{ID: uint(intID)}})
	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error occured while removing user",
		}
	}
	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: "User Deleted",
	}, nil
}

func (s service) UserExists(id uint) (bool, error) {
	return s.userPersistence.UserExists(model.User{Model: gorm.Model{ID: id}, Status: model.ACTIVE})
}
