package user

import (
	"blogapp/internal/constant"
	model "blogapp/internal/constant/model"
	"blogapp/internal/storage/persistence"
)

type UseCase interface {
	GetUser(userID string) (*constant.SuccessData, *constant.ErrorData)
	GetByEmail(email string) (*model.User, error)
	UpdateUser(user model.User) (*constant.SuccessData, *constant.ErrorData)
	StoreUser(user model.User) (*model.User, error)
	GetUsers() (*constant.SuccessData, *constant.ErrorData)
	DeleteUser(userID string) (*constant.SuccessData, *constant.ErrorData)
	UserExists(userID uint) (bool, error)
}

type service struct {
	userPersistence persistence.UserPersistence
}

func Initialize(
	userPersistence persistence.UserPersistence,
) UseCase {
	return &service{
		userPersistence: userPersistence,
	}
}
