package sub

import (
	"blogapp/internal/constant"
	"blogapp/internal/constant/model"

	"net/http"

	"strconv"

	"gorm.io/gorm"
)

func (s service) GetSubscription(subscriptionID string) (*constant.SuccessData, *constant.ErrorData) {
	intSubscriptionID, converstionError := strconv.Atoi(subscriptionID)

	if converstionError != nil || intSubscriptionID == 0 {
		return nil, &constant.ErrorData{
			Code:  http.StatusBadRequest,
			Title: "Error converting ID parameter",
		}
	}

	subscription, err := s.subscriptionPersistence.Subscription(model.Subscription{Model: gorm.Model{ID: uint(intSubscriptionID)}})
	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Reading Subscription",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: subscription,
	}, nil
}

func (s service) GetSubscriptions() (*constant.SuccessData, *constant.ErrorData) {
	subscriptions, err := s.subscriptionPersistence.Subscriptions()

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Getting Subscriptions",
		}
	}

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Getting Subscriptions",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: subscriptions,
	}, nil
}

func (s service) UpdateSubscription(subscription model.Subscription) (*constant.SuccessData, *constant.ErrorData) {
	_, err := s.subscriptionPersistence.UpdateSubscription(subscription)

	if err != nil {

		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error Occurred While Updating Subscription",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: "Subscription Updated",
	}, nil
}

func (s service) StoreSubscription(subscription model.Subscription) (*constant.SuccessData, *constant.ErrorData) {
	detail, err := s.subscriptionPersistence.StoreSubscription(subscription)

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error occurred while adding detail",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: *detail,
	}, nil
}

func (s service) DeleteSubscription(id string) (*constant.SuccessData, *constant.ErrorData) {
	intID, _ := strconv.Atoi(id)

	err := s.subscriptionPersistence.DeleteSubscription(model.Subscription{Model: gorm.Model{ID: uint(intID)}})

	if err != nil {
		return nil, &constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Error occured while removing subscription",
		}
	}

	return &constant.SuccessData{
		Code: http.StatusOK,
		Data: "Subscription Deleted",
	}, nil
}
