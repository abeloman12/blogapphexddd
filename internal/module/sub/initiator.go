package sub

import (
	"blogapp/internal/constant"
	model "blogapp/internal/constant/model"
	"blogapp/internal/storage/persistence"
)

type UseCase interface {
	GetSubscription(subscriptionID string) (*constant.SuccessData, *constant.ErrorData)
	UpdateSubscription(subscription model.Subscription) (*constant.SuccessData, *constant.ErrorData)
	StoreSubscription(subscription model.Subscription) (*constant.SuccessData, *constant.ErrorData)
	GetSubscriptions() (*constant.SuccessData, *constant.ErrorData)
	DeleteSubscription(subscriptionID string) (*constant.SuccessData, *constant.ErrorData)
}

type service struct {
	subscriptionPersistence persistence.SubscriptionPersistence
}

func Initialize(
	subscriptionPersistence persistence.SubscriptionPersistence,
) UseCase {
	return &service{
		subscriptionPersistence: subscriptionPersistence,
	}
}
