package sub

import (
	"blogapp/internal/constant"
	subscriptionmodel "blogapp/internal/constant/model"
	subscriptionmodule "blogapp/internal/module/sub"
	"strconv"

	"net/http"

	"github.com/gin-gonic/gin"
)

type SubscriptionHandler interface {
	AddSubscription(c *gin.Context)
	GetSubscriptions(c *gin.Context)
	GetSubscriptionById(c *gin.Context)
	UpdateSubscription(c *gin.Context)
	DeleteSubscription(c *gin.Context)
}

type subscriptionHandler struct {
	subscriptionUseCase subscriptionmodule.UseCase
}

func NewSubscriptionHandler(subscriptionUseCase subscriptionmodule.UseCase) SubscriptionHandler {
	return &subscriptionHandler{subscriptionUseCase: subscriptionUseCase}
}

func (h subscriptionHandler) AddSubscription(c *gin.Context) {
	var subscription subscriptionmodel.Subscription

	if err := c.ShouldBindJSON(&subscription); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	successData, errorData := h.subscriptionUseCase.StoreSubscription(subscription)
	if errorData != nil {
		constant.RespondWithError(c, constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Internal Server Error",
		})
	}

	constant.RespondWithSuccess(c, *successData)

}

func (h subscriptionHandler) GetSubscriptions(c *gin.Context) {
	successData, errData := h.subscriptionUseCase.GetSubscriptions()

	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}

func (h subscriptionHandler) GetSubscriptionById(c *gin.Context) {
	id := c.Param("subscription")
	successData, errData := h.subscriptionUseCase.GetSubscription(id)

	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}

func (h subscriptionHandler) UpdateSubscription(c *gin.Context) {
	var subscription subscriptionmodel.Subscription
	if err := c.ShouldBindJSON(&subscription); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	id := c.Param("subscription")
	intID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
	subscription.ID = uint(intID)
	successData, errData := h.subscriptionUseCase.UpdateSubscription(subscription)
	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}

func (h subscriptionHandler) DeleteSubscription(c *gin.Context) {
	id := c.Param("subscription")

	successData, errData := h.subscriptionUseCase.DeleteSubscription(id)
	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}
