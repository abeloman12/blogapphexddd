package user

import (
	"blogapp/internal/constant"
	usermodel "blogapp/internal/constant/model"
	usermodule "blogapp/internal/module/user"
	"blogapp/internal/utils"
	"strconv"

	"fmt"
	"net/http"
	"strings"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
)

type UserHandler interface {
	AddUser(enforcer *casbin.Enforcer) gin.HandlerFunc
	SignInUser(c *gin.Context)
	GetUsers(c *gin.Context)
	GetUserById(c *gin.Context)
	UpdateUser(c *gin.Context)
	DeleteUser(c *gin.Context)
}

type userHandler struct {
	userUseCase usermodule.UseCase
}

func NewUserHandler(userUseCase usermodule.UseCase) UserHandler {
	return &userHandler{userUseCase: userUseCase}
}

func (h userHandler) AddUser(enforcer *casbin.Enforcer) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var user usermodel.User
		if err := ctx.ShouldBindJSON(&user); err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}

		utils.HashPassword(&user.Password)

		if !strings.EqualFold("SUPERADMIN", user.Role) &&
			!strings.EqualFold("AUTHOR", user.Role) &&
			!strings.EqualFold("READER", user.Role) {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": "Role can only be 'SUPERADMIN', 'AUTHOR' or 'READER'"})
			return
		}

		registeredUser, err := h.userUseCase.StoreUser(user)
		if err != nil {
			constant.RespondWithError(ctx, constant.ErrorData{
				Code:  http.StatusInternalServerError,
				Title: "Internal Server Error",
			})
		}
		enforcer.AddGroupingPolicy(fmt.Sprint(registeredUser.ID), user.Role)
		registeredUser.Password = ""
		ctx.JSON(http.StatusBadRequest, registeredUser)
	}
}

func (h userHandler) GetUsers(c *gin.Context) {
	successData, errData := h.userUseCase.GetUsers()

	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}

func (h userHandler) GetUserById(c *gin.Context) {
	id := c.Param("user")
	successData, errData := h.userUseCase.GetUser(id)

	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}

func (h userHandler) UpdateUser(c *gin.Context) {
	var user usermodel.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	id := c.Param("user")
	intID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
	user.ID = uint(intID)
	successData, errData := h.userUseCase.UpdateUser(user)
	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}

func (h userHandler) DeleteUser(c *gin.Context) {
	id := c.Param("user")

	successData, errData := h.userUseCase.DeleteUser(id)
	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}

func (h userHandler) SignInUser(c *gin.Context) {
	var user usermodel.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	userResponse, error := h.userUseCase.GetByEmail(user.Email)
	if error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"msg": "Error finding user by email"})
		return
	}

	isTrue := utils.ComparePassword(userResponse.Password, user.Password)
	isActive, _ := h.userUseCase.UserExists(userResponse.ID)

	fmt.Println(userResponse.Password)
	fmt.Println(user.Password)
	fmt.Println(isTrue)
	fmt.Println(isActive)

	if isTrue && isActive {
		token := utils.GenerateToken(userResponse.ID)
		c.JSON(http.StatusOK, gin.H{"msg": "Successfully SignIN", "token": token})
		return
	}
	c.JSON(http.StatusInternalServerError, gin.H{"msg": "Password not matched"})
}
