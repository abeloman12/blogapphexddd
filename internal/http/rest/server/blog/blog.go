package blog

import (
	"blogapp/internal/constant"
	blogmodel "blogapp/internal/constant/model"
	blogmodule "blogapp/internal/module/blog"
	"path/filepath"
	"strconv"

	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

type BlogHandler interface {
	AddBlog(c *gin.Context)
	GetBlogs(c *gin.Context)
	GetBlogById(c *gin.Context)
	UpdateBlog(c *gin.Context)
	AddBlogImages(c *gin.Context)
	RemoveBlogImages(c *gin.Context)
	DeleteBlog(c *gin.Context)
}

type blogHandler struct {
	blogUseCase blogmodule.UseCase
}

func NewBlogHandler(blogUseCase blogmodule.UseCase) BlogHandler {
	return &blogHandler{blogUseCase: blogUseCase}
}

func (h blogHandler) AddBlog(c *gin.Context) {
	var blog blogmodel.Blog

	if err := c.ShouldBindJSON(&blog); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	successData, errorData := h.blogUseCase.StoreBlog(blog)
	if errorData != nil {
		constant.RespondWithError(c, constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: "Internal Server Error",
		})
	}

	constant.RespondWithSuccess(c, *successData)

}

func (h blogHandler) GetBlogs(c *gin.Context) {
	successData, errData := h.blogUseCase.GetBlogs()

	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}

func (h blogHandler) GetBlogById(c *gin.Context) {
	id := c.Param("blog")
	blog, err := h.blogUseCase.GetBlog(id)

	if err != nil {
		constant.RespondWithError(c, constant.ErrorData{
			Code:  http.StatusInternalServerError,
			Title: err.Error(),
		})
		return
	}
	constant.RespondWithSuccess(c, constant.SuccessData{
		Code: http.StatusOK,
		Data: blog,
	})
}

func (h blogHandler) UpdateBlog(c *gin.Context) {
	var blog blogmodel.Blog
	if err := c.ShouldBindJSON(&blog); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	id := c.Param("blog")
	intID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
	blog.ID = uint(intID)
	successData, errData := h.blogUseCase.UpdateBlog(blog)
	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}

func (h blogHandler) AddBlogImages(c *gin.Context) {
	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	images := form.File["images[]"]
	var imageFileNames []string
	for _, image := range images {
		extension := filepath.Ext(image.Filename)
		newFileName := uuid.New().String() + extension

		if err := c.SaveUploadedFile(image, "assets/"+newFileName); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		}

		imageFileNames = append(imageFileNames, newFileName)
	}
	var blog blogmodel.Blog

	intID, err := strconv.Atoi(c.Param("blog"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	blog.ID = uint(intID)
	blog.Images = imageFileNames

	successData, errData := h.blogUseCase.AddBlogImages(blog)
	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}

func (h blogHandler) RemoveBlogImages(c *gin.Context) {
	var blog blogmodel.Blog

	if err := c.ShouldBindJSON(&blog); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	intID, err := strconv.Atoi(c.Param("blog"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}

	blog.ID = uint(intID)

	successData, errData := h.blogUseCase.RemoveBlogImages(blog)
	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)

}

func (h blogHandler) DeleteBlog(c *gin.Context) {
	id := c.Param("blog")

	successData, errData := h.blogUseCase.DeleteBlog(id)
	if errData != nil {
		constant.RespondWithError(c, *errData)
		return
	}
	constant.RespondWithSuccess(c, *successData)
}
