package persistence

import (
	"blogapp/internal/constant/model"

	"github.com/vcraescu/go-paginator/v2"
	"gorm.io/gorm"
)

type BlogPersistence interface {
	Blog(param model.Blog) (*model.Blog, error)
	Blogs() ([]model.Blog, error)
	SearchBlogs(param model.Blog, sort string, page, size int) ([]model.Blog, paginator.Paginator, error)
	UpdateBlog(blog model.Blog) (*model.Blog, error)
	DeleteBlog(param model.Blog) error
	StoreBlog(blog model.Blog) (*model.Blog, error)
	AddBlogImages(blog model.Blog) (*model.Blog, error)
	RemoveBlogImages(blog model.Blog) (*model.Blog, error)
	BlogExists(param model.Blog) (bool, error)
	MigrateBlog() error
}

type blogPersistence struct {
	DB *gorm.DB
}

func BlogInit(db *gorm.DB) BlogPersistence {
	return &blogPersistence{
		DB: db,
	}
}

func (u blogPersistence) Blog(param model.Blog) (*model.Blog, error) {
	blog := &model.Blog{}

	err := u.DB.Model(&model.Blog{}).Where(&param).First(blog).Error
	if err != nil {
		return nil, err
	}
	return blog, nil
}

func (u blogPersistence) Blogs() (blogs []model.Blog, err error) {
	return blogs, u.DB.Find(&blogs).Error
}

func (u blogPersistence) SearchBlogs(param model.Blog, sort string, page, size int) ([]model.Blog, paginator.Paginator, error) {
	return nil, nil, nil
}

func (u blogPersistence) UpdateBlog(blog model.Blog) (*model.Blog, error) {
	err := u.DB.Model(&model.Blog{}).Where(&model.Blog{Model: gorm.Model{ID: blog.ID}}).Updates(&blog).Error
	if err != nil {
		return nil, err
	}
	return &blog, nil
}

func (u blogPersistence) DeleteBlog(param model.Blog) error {
	err := u.DB.Model(&model.Blog{}).Where(&param).Delete(&param).Error
	if err != nil {
		return err
	}
	return nil
}
func (u blogPersistence) StoreBlog(blog model.Blog) (*model.Blog, error) {
	err := u.DB.Model(&model.Blog{}).Create(&blog).Error
	if err != nil {
		return nil, err
	}
	return &blog, nil
}

func (u blogPersistence) AddBlogImages(param model.Blog) (*model.Blog, error) {
	blog := model.Blog{}

	tx := u.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return nil, err
	}

	if err := tx.First(&blog, model.Blog{Model: gorm.Model{ID: param.ID}}).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	//convert pq.StringArray to []string
	var imageFileNames = []string(blog.Images)
	var paramImagesAsArray = []string(param.Images)

	imageFileNames = append(imageFileNames, paramImagesAsArray...)
	blog.Images = imageFileNames

	if err := tx.Model(&model.Blog{}).Where(&model.Blog{Model: gorm.Model{ID: blog.ID}}).Updates(&blog).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	if transactionError := tx.Commit().Error; transactionError != nil {
		return nil, transactionError
	}

	return &blog, nil
}

func (u blogPersistence) RemoveBlogImages(param model.Blog) (*model.Blog, error) {
	blog := model.Blog{}

	tx := u.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return nil, err
	}

	if err := tx.First(&blog, model.Blog{Model: gorm.Model{ID: param.ID}}).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	//convert pq.StringArray to []string
	var imageFileNames = []string(blog.Images)
	var paramImagesAsArray = []string(param.Images)

	for i, storedImage := range imageFileNames {
		for _, imageToBeRemoved := range paramImagesAsArray {
			if imageToBeRemoved == storedImage {
				imageFileNames = append(imageFileNames[:i], imageFileNames[i+1:]...)
			}
		}
	}

	blog.Images = imageFileNames

	if err := tx.Model(&model.Blog{}).Where(&model.Blog{Model: gorm.Model{ID: blog.ID}}).Updates(&blog).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	if transactionError := tx.Commit().Error; transactionError != nil {
		return nil, transactionError
	}

	return &blog, nil
}

func (u blogPersistence) BlogExists(param model.Blog) (bool, error) {
	var count int64 = 0
	err := u.DB.Model(&model.Blog{}).Where(&param).Count(&count).Error
	if err != nil {
		return false, err
	}
	return count > 0, nil
}

func (u blogPersistence) MigrateBlog() error {
	return u.DB.AutoMigrate(&model.Blog{})
}
