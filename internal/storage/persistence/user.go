package persistence

import (
	"blogapp/internal/constant/model"

	"github.com/vcraescu/go-paginator/v2"
	"gorm.io/gorm"
)

type UserPersistence interface {
	User(param model.User) (*model.User, error)
	Users() ([]model.User, error)
	SearchUsers(param model.User, sort string, page, size int) ([]model.User, paginator.Paginator, error)
	UpdateUser(user model.User) (*model.User, error)
	DeleteUser(param model.User) error
	StoreUser(user model.User) (*model.User, error)
	UserExists(param model.User) (bool, error)
	EmailExists(email string) (bool, error)
	MigrateUser() error
}

type userPersistence struct {
	DB *gorm.DB
}

func UserInit(db *gorm.DB) UserPersistence {
	return &userPersistence{
		DB: db,
	}
}

func (u userPersistence) User(param model.User) (*model.User, error) {
	user := &model.User{}

	err := u.DB.Model(&model.User{}).Where(&param).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (u userPersistence) Users() (users []model.User, err error) {
	return users, u.DB.Find(&users).Error
}

func (u userPersistence) SearchUsers(param model.User, sort string, page, size int) ([]model.User, paginator.Paginator, error) {
	return nil, nil, nil
}

func (u userPersistence) UpdateUser(user model.User) (*model.User, error) {
	err := u.DB.Model(&model.User{}).Where(&model.User{Model: gorm.Model{ID: user.ID}}).Or(&model.User{Email: user.Email}).Updates(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (u userPersistence) DeleteUser(param model.User) error {
	err := u.DB.Model(&model.User{}).Where(&param).Delete(&param).Error
	if err != nil {
		return err
	}
	return nil
}
func (u userPersistence) StoreUser(user model.User) (*model.User, error) {
	err := u.DB.Model(&model.User{}).Create(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}
func (u userPersistence) UserExists(param model.User) (bool, error) {
	var count int64 = 0
	err := u.DB.Model(&model.User{}).Where(&param).Count(&count).Error
	if err != nil {
		return false, err
	}
	return count > 0, nil
}
func (u userPersistence) EmailExists(email string) (bool, error) {
	var count int64 = 0
	err := u.DB.Model(&model.User{}).Where(&model.User{Email: email}).Count(&count).Error
	if err != nil {
		return false, err
	}
	return count > 0, nil
}
func (u userPersistence) MigrateUser() error {
	return u.DB.AutoMigrate(&model.User{})
}
