package persistence

import (
	"blogapp/internal/constant/model"

	"github.com/vcraescu/go-paginator/v2"
	"gorm.io/gorm"
)

type SubscriptionPersistence interface {
	Subscription(param model.Subscription) (*model.Subscription, error)
	Subscriptions() ([]model.Subscription, error)
	SearchSubscriptions(param model.Subscription, sort string, page, size int) ([]model.Subscription, paginator.Paginator, error)
	UpdateSubscription(subscription model.Subscription) (*model.Subscription, error)
	DeleteSubscription(param model.Subscription) error
	StoreSubscription(subscription model.Subscription) (*model.Subscription, error)
	SubscriptionExists(param model.Subscription) (bool, error)
	SubscriberCount(param model.Subscription) (int64, error)
	SubscriptionCount(param model.Subscription) (int64, error)
	MigrateSubscription() error
}

type subscriptionPersistence struct {
	DB *gorm.DB
}

func SubscriptionInit(db *gorm.DB) SubscriptionPersistence {
	return &subscriptionPersistence{
		DB: db,
	}
}

func (u subscriptionPersistence) Subscription(param model.Subscription) (*model.Subscription, error) {
	subscription := &model.Subscription{}

	err := u.DB.Model(&model.Subscription{}).Where(&param).First(subscription).Error
	if err != nil {
		return nil, err
	}
	return subscription, nil
}

func (u subscriptionPersistence) Subscriptions() (subscriptions []model.Subscription, err error) {
	return subscriptions, u.DB.Find(&subscriptions).Error
}

func (u subscriptionPersistence) SearchSubscriptions(param model.Subscription, sort string, page, size int) ([]model.Subscription, paginator.Paginator, error) {
	return nil, nil, nil
}

func (u subscriptionPersistence) UpdateSubscription(subscription model.Subscription) (*model.Subscription, error) {
	err := u.DB.Model(&model.Subscription{}).Where(&model.Subscription{Model: gorm.Model{ID: subscription.ID}}).Updates(&subscription).Error
	if err != nil {
		return nil, err
	}
	return &subscription, nil
}

func (u subscriptionPersistence) DeleteSubscription(param model.Subscription) error {
	err := u.DB.Model(&model.Subscription{}).Where(&param).Delete(&param).Error
	if err != nil {
		return err
	}
	return nil
}
func (u subscriptionPersistence) StoreSubscription(subscription model.Subscription) (*model.Subscription, error) {
	subscriber := model.User{}
	author := model.User{}

	tx := u.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return nil, err
	}

	if err := tx.First(&subscriber, model.User{Model: gorm.Model{ID: uint(subscription.SubscriberID)}}).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	if err := tx.Where(model.User{Model: gorm.Model{ID: uint(subscription.AuthorID)}, Role: model.AUTHOR}).Or(model.User{Model: gorm.Model{ID: uint(subscription.AuthorID)}, Role: model.SUPERADMIN}).First(&author).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	if err := tx.Create(&subscription).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	if transactionError := tx.Commit().Error; transactionError != nil {
		return nil, transactionError
	}

	return &subscription, nil
}

func (u subscriptionPersistence) SubscriptionExists(param model.Subscription) (bool, error) {
	var count int64 = 0
	err := u.DB.Model(&model.Subscription{}).Where(&param).Count(&count).Error
	if err != nil {
		return false, err
	}
	return count > 0, nil
}

func (u subscriptionPersistence) SubscriberCount(param model.Subscription) (int64, error) {
	var count int64 = 0
	if err := u.DB.Model(&model.Subscription{}).Where(model.Subscription{AuthorID: param.AuthorID}).Count(&count).Error; err != nil {
		return 0, err
	}

	return count, nil
}

func (u subscriptionPersistence) SubscriptionCount(param model.Subscription) (int64, error) {
	var count int64 = 0
	if err := u.DB.Model(&model.Subscription{}).Where(model.Subscription{SubscriberID: param.SubscriberID}).Count(&count).Error; err != nil {
		return 0, err
	}

	return count, nil
}

func (u subscriptionPersistence) MigrateSubscription() error {
	return u.DB.AutoMigrate(&model.Subscription{})
}
