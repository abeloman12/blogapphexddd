package persistence

import (
	"blogapp/internal/constant/model"

	"github.com/vcraescu/go-paginator/v2"
	"gorm.io/gorm"
)

type BillingPersistence interface {
	Billing(param model.Billing) (*model.Billing, error)
	Billings() ([]model.Billing, error)
	SearchBillings(param model.Billing, sort string, page, size int) ([]model.Billing, paginator.Paginator, error)
	UpdateBilling(billing model.Billing) (*model.Billing, error)
	DeleteBilling(param model.Billing) error
	StoreBilling(billing model.Billing) (*model.Billing, error)
	BillingExists(param model.Billing) (bool, error)
	MigrateBilling() error
}

type billingPersistence struct {
	DB *gorm.DB
}

func BillingInit(db *gorm.DB) BillingPersistence {
	return &billingPersistence{
		DB: db,
	}
}

func (u billingPersistence) Billing(param model.Billing) (*model.Billing, error) {
	billing := &model.Billing{}

	err := u.DB.Model(&model.Billing{}).Where(&param).First(billing).Error
	if err != nil {
		return nil, err
	}
	return billing, nil
}

func (u billingPersistence) Billings() (billings []model.Billing, err error) {
	return billings, u.DB.Find(&billings).Error
}

func (u billingPersistence) SearchBillings(param model.Billing, sort string, page, size int) ([]model.Billing, paginator.Paginator, error) {
	return nil, nil, nil
}

func (u billingPersistence) UpdateBilling(billing model.Billing) (*model.Billing, error) {
	err := u.DB.Model(&model.Billing{}).Where(&model.Billing{Model: gorm.Model{ID: billing.ID}}).Updates(&billing).Error
	if err != nil {
		return nil, err
	}
	return &billing, nil
}

func (u billingPersistence) DeleteBilling(param model.Billing) error {
	err := u.DB.Model(&model.Billing{}).Where(&param).Delete(&param).Error
	if err != nil {
		return err
	}
	return nil
}
func (u billingPersistence) StoreBilling(billing model.Billing) (*model.Billing, error) {
	user := model.User{}

	tx := u.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return nil, err
	}

	if err := tx.First(&user, model.User{Model: gorm.Model{ID: uint(billing.UserID)}}).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	if err := tx.Create(&billing).Error; err != nil {
		tx.Rollback()
		return nil, err
	}

	if transactionError := tx.Commit().Error; transactionError != nil {
		return nil, transactionError
	}

	return &billing, nil
}

func (u billingPersistence) BillingExists(param model.Billing) (bool, error) {
	var count int64 = 0
	err := u.DB.Model(&model.Billing{}).Where(&param).Count(&count).Error
	if err != nil {
		return false, err
	}
	return count > 0, nil
}

func (u billingPersistence) MigrateBilling() error {
	return u.DB.AutoMigrate(&model.Billing{})
}
