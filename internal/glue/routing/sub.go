package routing

import (
	"blogapp/internal/http/rest/server/sub"
	"blogapp/internal/middleware"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
)

func SetUpSubRoutes(apiRoutes *gin.RouterGroup, enforcer *casbin.Enforcer, subHandler sub.SubscriptionHandler) *gin.RouterGroup {
	subRoutes := apiRoutes.Group("/subs", middleware.AuthorizeJWT())
	{
		subRoutes.GET("/", subHandler.GetSubscriptions)
		subRoutes.GET("/:subscription", subHandler.GetSubscriptionById)
		subRoutes.PUT("/:subscription", subHandler.UpdateSubscription)
		subRoutes.DELETE("/:subscription", subHandler.DeleteSubscription)
		subRoutes.POST("/", subHandler.AddSubscription)
	}

	return subRoutes
}
