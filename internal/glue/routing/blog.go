package routing

import (
	"blogapp/internal/http/rest/server/blog"
	"blogapp/internal/middleware"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
)

func SetUpBlogRoutes(apiRoutes *gin.RouterGroup, enforcer *casbin.Enforcer, blogHandler blog.BlogHandler) *gin.RouterGroup {
	blogProtectedRoutes := apiRoutes.Group("/blogs", middleware.AuthorizeJWT())
	{
		blogProtectedRoutes.GET("/", middleware.Authorize("blog", "read", enforcer), blogHandler.GetBlogs)
		blogProtectedRoutes.GET("/:blog", middleware.Authorize("blog", "read", enforcer), blogHandler.GetBlogById)
		blogProtectedRoutes.PUT("/:blog", middleware.Authorize("blog", "write", enforcer), blogHandler.UpdateBlog)
		blogProtectedRoutes.DELETE("/:blog", middleware.Authorize("blog", "write", enforcer), blogHandler.DeleteBlog)
		blogProtectedRoutes.POST("/", middleware.Authorize("blog", "write", enforcer), blogHandler.AddBlog)
		blogProtectedRoutes.PUT("/images/:blog", middleware.Authorize("blog", "write", enforcer), blogHandler.AddBlogImages)
		blogProtectedRoutes.DELETE("/images/:blog", middleware.Authorize("blog", "write", enforcer), blogHandler.RemoveBlogImages)
	}

	return blogProtectedRoutes
}
