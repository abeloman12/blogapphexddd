package routing

import (
	"blogapp/internal/http/rest/server/blog"
	"blogapp/internal/http/rest/server/sub"
	"blogapp/internal/http/rest/server/user"
	"fmt"

	"github.com/casbin/casbin/v2"
	gormadapter "github.com/casbin/gorm-adapter/v3"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type AppHandler struct {
	UserHandler user.UserHandler
	BlogHandler blog.BlogHandler
	SubHandler  sub.SubscriptionHandler
}

func SetupRoutes(db *gorm.DB, handler AppHandler) {
	httpRouter := gin.Default()

	adapter, err := gormadapter.NewAdapterByDB(db)

	if err != nil {
		panic(fmt.Sprintf("failed to initialize casbin adapter: %v", err))
	}

	enforcer, err := casbin.NewEnforcer("config/rbac_model.conf", adapter)
	if err != nil {
		panic(fmt.Sprintf("failed to create casbin enforcer: %v", err))
	}

	if hasPolicy := enforcer.HasPolicy("SUPERADMIN", "user", "read"); !hasPolicy {
		enforcer.AddPolicy("SUPERADMIN", "user", "read")
	}

	if hasPolicy := enforcer.HasPolicy("SUPERADMIN", "user", "write"); !hasPolicy {
		enforcer.AddPolicy("SUPERADMIN", "user", "write")
	}

	if hasPolicy := enforcer.HasPolicy("SUPERADMIN", "blog", "read"); !hasPolicy {
		enforcer.AddPolicy("SUPERADMIN", "blog", "read")
	}

	if hasPolicy := enforcer.HasPolicy("SUPERADMIN", "blog", "write"); !hasPolicy {
		enforcer.AddPolicy("SUPERADMIN", "blog", "write")
	}

	if hasPolicy := enforcer.HasPolicy("AUTHOR", "blog", "read"); !hasPolicy {
		enforcer.AddPolicy("AUTHOR", "blog", "read")
	}

	if hasPolicy := enforcer.HasPolicy("AUTHOR", "blog", "write"); !hasPolicy {
		enforcer.AddPolicy("AUTHOR", "blog", "write")
	}

	if hasPolicy := enforcer.HasPolicy("READER", "blog", "read"); !hasPolicy {
		enforcer.AddPolicy("READER", "blog", "read")
	}

	apiRoutes := httpRouter.Group("/api")

	{
		apiRoutes.POST("/register", handler.UserHandler.AddUser(enforcer))
		apiRoutes.POST("/signin", handler.UserHandler.SignInUser)
	}

	SetUpUserRoutes(apiRoutes, enforcer, handler.UserHandler)
	SetUpBlogRoutes(apiRoutes, enforcer, handler.BlogHandler)
	SetUpSubRoutes(apiRoutes, enforcer, handler.SubHandler)

	httpRouter.Run()
}
