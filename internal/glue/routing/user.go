package routing

import (
	"blogapp/internal/http/rest/server/user"
	"blogapp/internal/middleware"

	"github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
)

func SetUpUserRoutes(apiRoutes *gin.RouterGroup, enforcer *casbin.Enforcer, userHandler user.UserHandler) *gin.RouterGroup {
	userProtectedRoutes := apiRoutes.Group("/users", middleware.AuthorizeJWT())
	{
		userProtectedRoutes.GET("/", middleware.Authorize("user", "read", enforcer), userHandler.GetUsers)
		userProtectedRoutes.GET("/:user", middleware.Authorize("user", "read", enforcer), userHandler.GetUserById)
		userProtectedRoutes.PUT("/:user", middleware.Authorize("user", "write", enforcer), userHandler.UpdateUser)
		userProtectedRoutes.DELETE("/:user", middleware.Authorize("user", "write", enforcer), userHandler.DeleteUser)
	}

	return userProtectedRoutes
}
