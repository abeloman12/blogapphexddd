package model

import "gorm.io/gorm"

const (
	SUPERADMIN string = "SUPERADMIN"
	AUTHOR     string = "AUTHOR"
	READER     string = "READER"
)

const (
	ACTIVE   string = "ACTIVE"
	INACTIVE string = "INACTIVE"
)

type User struct {
	gorm.Model
	Name     string  `json:"name" form:"name"`
	Email    string  `json:"email" form:"email" gorm:"unique;not null"`
	Role     string  `json:"role" form:"role"	gorm:"not null"`
	Password string  `json:"password" form:"password"`
	Bill     float32 `json:"bill" form:"bill"`
	Status   string  `json:"status" form:"status" gorm:"default:ACTIVE"`
}

//TableName --> Table for User Model
func (User) TableName() string {
	return "users"
}
