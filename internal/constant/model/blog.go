package model

import (
	"github.com/lib/pq"
	"gorm.io/gorm"
)

type Blog struct {
	gorm.Model
	Title   string         `json:"title" form:"title"`
	Content string         `json:"content" form:"content"`
	Images  pq.StringArray `json:"images" gorm:"type:text[]"`
	UserID  int            `json:"user_id"`
	User    User
}

//TableName --> Table for Blog Model
func (Blog) TableName() string {
	return "blogs"
}
