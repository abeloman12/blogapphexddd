package model

import (
	"gorm.io/gorm"
)

type Subscription struct {
	gorm.Model
	SubscriberID int  `json:"subscriber_id"`
	AuthorID     int  `json:"author_id"`
	Subscriber   User `gorm:"foreignKey:SubscriberID"`
	Author       User `gorm:"foreignKey:AuthorID"`
}

//TableName --> Table for Blog Model
func (Subscription) TableName() string {
	return "subscriptions"
}
