package model

import (
	"gorm.io/gorm"
)

const BASEPRICE int64 = 200
const SUBPRICE int64 = 5

type Billing struct {
	gorm.Model
	Month  string  `json:"month" form:"month"`
	Year   int     `json:"year" form:"year"`
	UserID int     `json:"user_id"`
	Amount float32 `json:"amount" form:"amount"`
	User   User
}

//TableName --> Table for Blog Model
func (Billing) TableName() string {
	return "billings"
}
