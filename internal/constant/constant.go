package constant

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
)

type Sort []string

type QueryParams struct {
	Sort    string `form:"sort"`
	Filter  string `form:"filter"`
	Page    string `form:"page"`
	PerPage string `form:"per_page"`
}

type Filter struct {
	Key    string
	Values []string
}

type FilterParams struct {
	Sort    Sort
	Filter  Filter
	Page    int64
	PerPage int64
}

type SuccessResponse struct {
	Success bool         `json:"success"`
	Data    *interface{} `json:"data,omitempty"`
}

type ErrorResponse struct {
	Success bool          `json:"success"`
	Errors  *ErrorMessage `json:"errors,omitempty"`
}

type ErrorMessage struct {
	Code   int    `json:"code"`
	Source string `json:"source"`
	Title  string `json:"title"`
}

type ErrorData struct {
	Code  int
	Title string
}

type SuccessData struct {
	Code int
	Data interface{}
}

func IsValidSort(key string) bool {
	return strings.EqualFold("ASC", key) || strings.EqualFold("DESC", key)
}

func GetHostNameString() (string, error) {
	return os.Getenv("HOST_NAME"), nil
}

type LinksData struct {
	Self     string `json:"self"`
	First    string `json:"first"`
	Previous string `json:"previous"`
	Next     string `json:"next"`
	Last     string `json:"last"`
}

func GetFormattedLinkType1(isValid bool, name string, page int, maxPerPage int, sort, host string) string {
	if isValid {
		return fmt.Sprintf("%s/%s/?page=%d&per_page=%d&sort=%s", host, name, page, maxPerPage, sort)
	}
	return fmt.Sprintf("%s/%s/?page=%d&per_page=%d", host, name, page, maxPerPage)

}

type MetaData struct {
	Page       int       `json:"page"`
	PerPage    int       `json:"per_page"`
	PageCount  int       `json:"page_count"`
	TotalCount int       `json:"total_count"`
	Links      LinksData `json:"links"`
}

func CreateMetaData(page int, perPage int, pageCount int, totalCount int, data LinksData) MetaData {
	return MetaData{
		Page:       page,
		PerPage:    perPage,
		PageCount:  pageCount,
		TotalCount: totalCount,
		Links:      data,
	}
}

type PaginatedData struct {
	MetaData MetaData    `json:"meta_data"`
	Data     interface{} `json:"data"`
}

func RespondWithError(c *gin.Context, err ErrorData) {

	errResp := ErrorResponse{
		Success: false,
		Errors: &ErrorMessage{
			Code:   err.Code,
			Source: c.Request.RequestURI,
			Title:  err.Title,
		},
	}
	c.JSON(err.Code, errResp)
}

func RespondErrorWithMessage(c *gin.Context, error error, message string) {

	errResp := ErrorResponse{
		Success: false,
		Errors: &ErrorMessage{
			Code:   http.StatusInternalServerError,
			Source: c.Request.RequestURI,
			Title:  message,
		},
	}
	c.JSON(http.StatusInternalServerError, errResp)
}

func RespondWithSuccess(c *gin.Context, successData SuccessData) {

	resp := SuccessResponse{
		Success: true,
		Data:    &successData.Data,
	}
	c.JSON(http.StatusOK, resp)
}
